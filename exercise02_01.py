import hashlib
import multiprocessing

#Dictonary für alle möglichen Zeichen die ein Passwort beinhalten kann
passwordEntrys = {1:"A",2:"B",3:"C",4:"D",5:"E",6:"F",7:"G",8:"H",9:"I",10:"J",11:"K",12:"L",13:"M",14:"N",15:"O",16:"P",17:"Q",18:"R",19:"S",20:"T",21:"U",22:"V",23:"W",24:"X",25:"Y",26:"Z",27:"a",28:"b",29:"c",30:"d",31:"e",32:"f",33:"g",34:"h",35:"i",36:"j",37:"k",38:"l",39:"m",40:"n",41:"o",42:"p",43:"q",44:"r",45:"s",46:"t",47:"u",48:"v",49:"w",50:"x",51:"y",52:"z",53:"0",54:"1",55:"2",56:"3",57:"4",58:"5",59:"6",60:"7",61:"8",62:"9"}

#Seed für die Passwort Hashes
seedForPasswords = "EArfTx8Pi78221jILzjr"

#Wandelt die Password INT Liste zu einem String um
def getPassword(iterationList):

    return (passwordEntrys[iterationList[0]]+passwordEntrys[iterationList[1]]+passwordEntrys[iterationList[2]]+passwordEntrys[iterationList[3]])

#Erhöht den letzten Integer um 1 und falls eine Zahl 62 erreicht wird der übertrag berücksichtigt, crashed falls der erste Wert 62 erreicht
def raiseList(intList):

    if intList[3] <= 61:
        intList[3] = intList[3]+1
        return intList
    else:
        intList[3] = 1
        if intList[2] <= 61:
            intList[2] = intList[2]+1
            return intList
        else:
            intList[2] = 1
            if intList[1] <= 61:
                intList[1] = intList[1]+1
                return intList
            else:
                intList[1] = 1
                if intList[0] <= 61:
                    intList[0] = intList[0]+1
                    return intList
                else:
                    return False

def bruteForcePassword(hashString):

    #Startliste für das Passwort
    passwordList = [1,1,1,1]
    #leeres Startpasswort
    word = "".encode('utf-8')

    while hashString != hashlib.sha256(word).hexdigest():
        #nächstes Passwort wird generiert
        word = (seedForPasswords + getPassword(passwordList)).encode('utf-8')
        #Passwort INT Liste wird um eins erhöht
        passwordList = raiseList(passwordList)

    #Hash und cleartext Passwort wird in ein TXT geschrieben
    passFile = open("passwordsFromHashes.txt","a+")
    passFile.write(hashString +" - "+ getPassword(passwordList) + "\n")
    passFile.close()

#Hash TXT wird geöffnet
hashFile = open('hash.txt','r')
hashFile.readline()
integer = 0
nameList = []

#Für jeden Hash(jede Zeile) wird ein Process gestartet, ermöglich große Parallelisierung, verkürzt Rechenzeit
for line in hashFile:
    name = "thread_" + str(integer)
    name = multiprocessing.Process(target=bruteForcePassword, args=(line.replace("\n",""),))
    nameList.append(name)
    nameList[integer].start()
    integer = integer + 1

#hash TXT wird geschlossen
hashFile.close()