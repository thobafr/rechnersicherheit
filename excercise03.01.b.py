import multiprocessing
import time
from matplotlib import scale
import matplotlib.pyplot as plt

maxPasswordLength = 275

def readInPasswords(fileToRead):

    bigList = []

    for line in fileToRead:
        akkList = line.split()
        if len(akkList) == 1:
            akkList.append("")
        akkList[1].replace("\n","")
        akkList[1].encode(encoding='Latin-1')
        bigList.append(akkList)
    
    return bigList

def getAccountAmount(bigList):

    accAmount = 0

    for element in bigList:
        accAmount += int(element[0])
    
    return accAmount

def getLength(bigList):

    lengthList = []

    for i in range(0,maxPasswordLength):
        lengthList.append([i,0])

    for element in bigList:
        try:
            lengthList[len(element[1])][1] += 1
        except:
            print(element)
    lengthList.sort(key = lambda x: x[1])

    for i in range(len(lengthList)-1,-1,-1):
        if lengthList[i][1] == 0:
            lengthList.remove(lengthList[i])
    lengthList.reverse()
    for i in range(0,len(lengthList)):
        plt.scatter(lengthList[i][0],lengthList[i][1])
    scale_factor = 1
    xmin, xmax = plt.xlim()
    plt.xlim(xmin * scale_factor, xmax * scale_factor)
    plt.ylabel("Vorkommen einer Passwortlänge in Millionen")
    plt.xlabel("Passwortlänge")
    plt.grid()
    plt.savefig('this2.png')

def getMostUsedSymbol(bigList):

    fileData = open("thisFile.txt","w", encoding="Latin-1")

    symbolDict = {}
    totalAmountOfSymbol = 0

    for element in bigList:
        for i in range(0,len(element[1])):
            if element[1][i] in symbolDict:
                akku = symbolDict[element[1][i]]
                symbolDict.update({element[1][i]:akku+1})
                totalAmountOfSymbol += 1
            else:
                symbolDict.update({element[1][i]:1})
                totalAmountOfSymbol += 1

    sortedList = sorted(symbolDict.items(), key = 
             lambda kv:(kv[1], kv[0]))
    sortedList.reverse() 
    print(sortedList)
    print(len(sortedList))

    fileData.write(str(sortedList))
    fileData.close()
    print(totalAmountOfSymbol)


testList = [[154,"thisYou"],[112,"loveIsAwesome"]]
fileRead = open("rockyou-withcount.txt",encoding="Latin-1")
bigList = readInPasswords(fileRead)
getLength(bigList)
getMostUsedSymbol(bigList)
print(getAccountAmount(bigList))

fileRead.close()