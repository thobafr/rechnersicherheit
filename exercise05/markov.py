fileName = "rockYou.txt"

def readIn(fileN):
    #Funktion zum zeilenweisen Einlesen
    pinList = []

    rFile = open(fileN,"r")
    for line in rFile:
        akku = line.split()
        pinList.append(akku[0])
    
    return pinList

def transitionPercentage(pinList):
    #Jeder Übergang wird in eine Matrix eingefügt -> Matrix[Von][Nach]

    matrix = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

    for element in pinList:
        for i in range(0,len(element)-1):
            matrix[int(element[i])][int(element[i+1])] += 1

    return matrix

def transformToMarkov(matrix):
    #Markov Modell wird erstellt, alle Übergangswahrscheinlichkeiten werden berechnet

    matrixMarkov = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]


    for i in range(0,len(matrix)):
        for m in range(0,len(matrix[i])):
            matrixMarkov[i][m] = float((100*matrix[i][m])/sum(matrix[i]))
        #Sollten alle Übergangswahrscheinlichkeiten nicht zwischen 99.9 und 100.1 Prozent liegen ist ein Fehler aufgetreten
        if sum(matrixMarkov[i]) < 99.9 or sum(matrixMarkov[i]) > 100.1:
            print("Error")
    
    return(matrixMarkov)

#Zwischenspeicher von Ergebnissen
pinList = readIn(fileName)
amountList = transitionPercentage(pinList)
markovModell = transformToMarkov(amountList)

#Ausgabe der Matrix des Markov Modells
for i in range(0,len(markovModell)):
    print(markovModell[i])