import socket
import time
import os
import threading, queue
import logging
import hashlib


class Chatserver:

        #static config
        amountConnections = 500
        new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        new_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_ip = '127.0.0.1'
        port = 8080

        #datastructures
        clientList = []
        accountDict = {}
        messageQueue = queue.Queue(amountConnections*10)
        writingQueue = queue.Queue(amountConnections*10)
        messageList = []
        privateMessageList = queue.Queue(amountConnections*10)
        dataQueue = queue.Queue(amountConnections)

        def __init__(self):
            #logging init
            logging.basicConfig(filename='app.log', format='%(asctime)s %(process)d-%(levelname)s-%(message)s', filemode='a', level=logging.NOTSET, datefmt='%Y-%m-%d %H:%M:%S')

            #readin useraccounts
            rFile = open("userFile.txt","r")
            for line in rFile:
                user = line.split()
                if user[0] not in self.accountDict.keys():
                    self.accountDict[user[0]] = user[1]
            rFile.close()
            logging.critical('Useraccount data read in complete')

            #readin in oldMessages
            messageFile = open("messageLog.txt","r")
            message = [0,0]
            for line in messageFile:
                akku = line.split()
                if message[0] == 0:
                    message[0] = akku[0]
                else:
                    message[1] = akku[0]
                    self.messageList.append(message)
                    message = [0,0]

            #starting/binding server
            logging.critical("Server is starting up")
            self.new_socket.bind((self.server_ip,self.port))
            logging.critical('Socket bound successfully')
            logging.critical("Starting server at IP: " + str(self.server_ip))
            self.new_socket.listen(self.amountConnections)
            
            #startingThreads
            beamThread = threading.Thread(None, target=self.beamingThread, name="beamingThread",kwargs=None)
            beamThread.start()
            logging.critical('beaming thread started')
            writer = threading.Thread(None, target=self.writingThread, name="writingThread",kwargs=None)
            writer.start()
            logging.critical('writing thread started')

            while True:
                # take all incomming connections and make a thread out of them
                conn, addr = self.new_socket.accept()
                logging.info('Received connection from: ' + str(addr))
                logging.info('Connection established successful. Connected from:  ' + str(addr))

                self.clientList.append([conn])
                t = threading.Thread(None, target=self.clientThread, name=addr[0], args=((conn, addr)),kwargs=None)
                t.start()
                logging.info("Client thread startet for: " + str(addr))

        def writingThread(self):
            #writes new users into the userfile

            while True:
                    if not self.writingQueue.empty():
                        message = self.writingQueue.get(block=False, timeout=None)
                        wFile = open("userFile.txt","a+")
                        wFile.write(str(message[1])+" "+str(message[2])+"\n")
                        wFile.close()
                        logging.info("Saved new account '" + str(message[1]) + "' to file")
                    else:
                        time.sleep(1)

        def clientThread(self,conn, addr):
            #each client thread listens to one client

            actionList = []

            try:
                message = "Connected to server \r\n"
                conn.send(message.encode('utf8'))
                message = conn.recv(2048).decode('utf8')
                actionList = message.split()
                logging.info(str(addr) + " invoces operation " + str(actionList[0]))
                if actionList[0] == "login":
                    if actionList[1] in self.accountDict.keys():
                        if actionList[2] == self.accountDict[actionList[1]]:
                            logging.info("successfull logged in account '" + str(actionList[1]) + "' with address " + str(addr))
                            for i in range(0,len(self.clientList)):
                                if self.clientList[i][0]==conn:
                                    self.clientList[i].append(str(actionList[1]))
                            pass
                        else:
                            logging.warning("tryed to log into account: '" + str(actionList[1]) + "' with false password")
                            message = "Failed to log into account, please reconnect to try again \n"
                            conn.send(message.encode('utf8'))
                            message = "incorrect_command"
                            conn.send(message.encode('utf8'))
                            self.clientList.remove(conn)
                            return
                    else:
                        return
                elif actionList[0] == "new" and actionList[1] != "" and actionList[2] != "":
                    if actionList[1] not in self.accountDict:
                        logging.info("created new account '"+ str(actionList[1]) + "'")
                        self.writingQueue.put(actionList)
                        self.accountDict[actionList[1]] = actionList[2]
                else:
                    print(actionList)
                    message = "You tryed to invoce an not supported action, please reconnect to try again"
                    conn.send(message.encode('utf8'))
                    message = "incorrect_command"
                    conn.send(message.encode('utf8'))
                    logging.warning(str(actionList[1]) + "tryed to invoce a false action: " + str(actionList[0]))
                    return
            except:
                logging.critical("User authentification failed")

            for i in range(0,len(self.messageList)):
                message = self.messageList[i][0] + " send Message: " + self.messageList[i][1] + "\n"
                conn.send(message.encode('utf8'))


            while(True):
                message = conn.recv(2048).decode('utf8')
                if len(message) == 0:
                    try:
                        self.clientList.remove(conn)
                        logging.info("user " + str(addr) + " disconnected")
                    except:
                        logging.info("user " + str(addr) + " disconnected with error")
                    break
                if message == "?sendFile?":
                    try:
                        fileReciever = conn.recv(2048).decode('utf8')
                        print(fileReciever)
                        fileNameOld = conn.recv(2048).decode('utf8')
                        logging.info(actionList[1] + " send file with name: " + fileNameOld + "to user " + str(fileReciever))
                        ending = fileNameOld.split(".")

                        hash_object = hashlib.sha1(bytes(actionList[1], 'utf-8'))
                        hashName = hash_object.hexdigest()
                        folder = "./"+hashName
                        ticks = time.time()
                        
                        fileName = str(ticks) + "_" + hashName
                        if not os.path.exists(folder):
                            logging.info("created folder " + str(folder))
                            os.mkdir(folder)
                            pathToWrite = folder+"/"+fileName
                        else:
                            logging.info("folder " + str(folder) + "already existed")
                            pathToWrite = folder+"/"+fileName
                        #end
                        pathToWrite = pathToWrite+"."+ending[1]
                        file = open(pathToWrite,"wb")
                        recData = conn.recv(2048)
                        while len(recData) == 2048:
                            file.write(recData)
                            recData = conn.recv(2048)
                        file.write(recData)
                        file.close()
                        logging.info("user '"+ actionList[1] +"' at "+ str(addr) + " send file: " + str(fileNameOld) + " to " + str(fileReciever))
                        self.dataQueue.put([pathToWrite,fileReciever,actionList[1]])
                    except:
                        logging.error("File transfer fail")
                elif ":" in message:
                    akku = message.split(":")
                    messageAkku = str(actionList[1]) + " send your a private Message: " + ":".join(akku[1:])
                    self.privateMessageList.put([akku[0],messageAkku])
                    logging.info("user '"+ actionList[1] +"' at "+ str(addr) + " send private message: " + str(messageAkku) + "to" + akku[0])
                else:
                    logging.info("user '"+ actionList[1] +"' at "+ str(addr) + " send message: " + str(message))
                    self.messageList.append([actionList[1],str(message)])
                    self.messageQueue.put((conn,actionList[1], message))

        def beamingThread(self):
            #sends the message to all connected threads

            while True:
                if not self.messageQueue.empty():
                    message = self.messageQueue.get(block=False, timeout=None)
                    conn = message[0]
                    message_to_send = message[1] + " send Message: " + message[2] + "\n"
                    for client in self.clientList:
                        try:
                            if client[0] != conn:
                                client[0].send(message_to_send.encode('utf8'))
                        except Exception as e:
                            logging.error(e)
                    messageFile = open("messageLog.txt","a+")
                    messageFile.write(message[1]+"\n")
                    messageFile.write(message[2]+"\n")
                    messageFile.close()
                elif not self.privateMessageList.empty():
                    try:
                        message = self.privateMessageList.get(block=False, timeout=None)
                        for client in self.clientList:
                            if client[1] == message[0]:
                                client[0].send(message[1].encode('utf8'))
                    except Exception as e:
                        print(e)

                        print("couldnt send private Message")
                elif not self.dataQueue.empty():
                    try:
                        message = self.dataQueue.get(block=False, timeout=None)
                        for client in self.clientList:
                            if client[1] == message[1]:
                                akkMessage = message[0].split("/")
                                akkString = "?sendFile?"+str(akkMessage[2])+"?"+message[2] + " send u a file with name " + message[0] +"?"
                                byteLen = 2048 - len(akkString.encode("utf8"))
                                akkString = akkString.encode("utf8") + bytes(byteLen)
                                client[0].send(akkString)
                                file = open(message[0],"rb")
                                sendData = file.read(2048)
                                while sendData:
                                    print(len(sendData))
                                    client[0].send(sendData)
                                    sendData = file.read(2048)
                                print("File send")
                                file.close()
                                print("fileSend")
                    except Exception as e:
                        print(e)
                        beamThread = threading.Thread(None, target=self.beamingThread, name="beamingThread",kwargs=None)
                        beamThread.start()
                        print("new thread started")
                        pass

                else:
                    time.sleep(1)


if __name__ == "__main__":
    server = Chatserver()