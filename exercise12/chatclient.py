import select
import socket
import select
import threading, queue
import hashlib
from time import time

class ChatClient:

	#static config
	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_ip = '127.0.0.1'
	client_ip = '127.0.0.2'
	sport = 8080
	running = True

	receiveQueue = queue.Queue(50)

	def sendThread(self):
		#thread for sending to server
		print("---> To close the chat use the command ?close? <---")
		print("To send a pivate message write username:message")
		print()
		while self.running:
			message = input()
			if message == "?close?":
				self.clientSocket.close()
				self.running = False
			elif message == "?sendFile?":
				try:
					self.clientSocket.send(message.encode('utf8'))
					print("Please input who the file is for:")
					otherUser = input()
					self.clientSocket.send(otherUser.encode('utf8'))
					print("Please input file path:")
					filePath = input()
					self.clientSocket.send(filePath.encode('utf8'))
					file = open(filePath,"rb")
					sendData = file.read(2048)
					while sendData:
						print(len(sendData))
						self.clientSocket.send(sendData)
						sendData = file.read(2048)
					print("File send")
					file.close()
				except:
					print("Error while sending file")
			else:
				self.clientSocket.send(message.encode('utf8'))
				
	def receiveThread(self):
		#thread for receiving from the server
		while self.running:
			try:
				ready = select.select([self.clientSocket], [], [], 0.1)
				if ready[0]:
					message = self.clientSocket.recv(2048)
					print("------ " + str(message.decode("utf8")))
					if "?sendFile?" in message.decode('utf8'):
						print(message.decode("utf8"))
						print("recieving file")
						akku = message.decode("utf8").split("?")
						#recData = self.clientSocket.recv(2048)
						#recData = recData.replace(message,b"")
						self.receiveQueue.put(message)
						print(akku)
						fileName = akku[2]
						#problem here
						print("filename: " + fileName)
						file = open("recieved-"+fileName,"wb")
						print("fileOpen")
						recData = self.clientSocket.recv(2048)
						print("got first chunk")
						#print(recData)
						while len(recData) == 2048:
							file.write(recData)
							recData = self.clientSocket.recv(2048)
							print(len(recData))
						file.write(recData)
						file.close()
						print("got file")
						#problem end
					else:
						self.receiveQueue.put(message)
			except Exception as e:
				print(e)
				self.running = False
				break

	def __init__(self):
		
		#connect to server
		self.clientSocket.connect((self.server_ip, self.sport))
		self.clientSocket.setblocking(0)

		#login/register process
		while True:
			print("Please choose 'new' to create a new User or 'login' to login into an existing one.")
			akkuText = input()
			print('', end='')
			if akkuText == "new":
				print("U cant use : in your username")
				print("Please input new username:")
				name_Input = input()
				if ":" in name_Input:
					print("U cannot use : in your username")
					continue
				print("Please input new password:")
				password_Input = input()
				password_Input = name_Input + password_Input
				messageHash = hashlib.sha256(password_Input.encode())

				message = (akkuText+" "+name_Input+" "+messageHash.hexdigest())
				
				serverMessage = self.clientSocket.recv(2048)
				self.receiveQueue.put(serverMessage)
				self.clientSocket.send(message.encode('utf8'))
				print()
				break

			elif akkuText == "login":
				print("Please input username:")
				name_Input = input()
				print("Please input password:")
				password_Input = input()
				password_Input = name_Input + password_Input
				messageHash = hashlib.sha256(password_Input.encode())

				message = (akkuText+" "+name_Input+" "+messageHash.hexdigest())
				
				serverMessage = self.clientSocket.recv(2048)
				self.receiveQueue.put(serverMessage)
				self.clientSocket.send(message.encode('utf8'))
				print()
				break
			
		#start handlingThreads
		sendThread = threading.Thread(None, target=self.sendThread, name="sendThread", kwargs=None)
		sendThread.start()
		receiveThread = threading.Thread(None, target=self.receiveThread, name="receiveThread", kwargs=None)
		receiveThread.start()

		#Print and froced quit if command was wrong
		while sendThread.is_alive() and receiveThread.is_alive():
			if not self.receiveQueue.empty():
				message = self.receiveQueue.get(block=False, timeout=None)
				if message.decode('utf8') == "incorrect_command":
					self.clientSocket.close()
					self.running = False
				if message.decode('utf8') == "":
					print("The server disconnected suddenly please try again")
					self.clientSocket.close()
					self.running = False
					break
				print(message.decode('utf8'))


if __name__ == "__main__":
	client = ChatClient()