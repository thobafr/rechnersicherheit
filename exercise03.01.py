import pandas as pd
from scipy.stats import entropy

def passSetClean(passFile):
    passFileOnlyPassword = []
    for i in passFile:
        tempSplit = i.split()
        if len(tempSplit) > 1:
            passFileOnlyPassword.append(tempSplit[1])
    p1SetAndp2Set(passFileOnlyPassword)


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def hasUpper(inputString):
    return any(char.isupper() for char in inputString)  

def hasSpecialChar(inputString):
    return any(not c.isalnum() for c in inputString)

def shannonEntropy(dataSet):
    pd_series = pd.Series(dataSet)
    counts = pd_series.value_counts()
    solution = entropy(counts)
    return solution


def p1SetAndp2Set(passFileOnlyPassword):
    p1PassSet = []
    p2PassSet = []
    for i in passFileOnlyPassword:
        if len(i)>6 and len(i)<32 and hasNumbers(i) and hasUpper(i):
            p1PassSet.append(i)
        if len(i)>8 and len(i)<32 and hasNumbers(i) and hasUpper(i) and hasSpecialChar(i):
            p2PassSet.append(i)
    writeFile(p1PassSet)
    writeFile(p2PassSet)
    print(shannonEntropy(p1PassSet))
    print(shannonEntropy(p2PassSet))  
    #print(p1PassSet)



def writeFile(p1PassSet):
    with open('passwordsP1Set.txt',"w", encoding="Latin-1") as f:
        for i in p1PassSet:
            f.write(i+ "\n")
        f.close()


def writeFile(p2PassSet):
    with open('passwordsP2Set.txt',"w", encoding="Latin-1") as f:
        for i in p2PassSet:
            f.write(i+ "\n")
        f.close()


with open('rockyou-withcount.txt', encoding="Latin-1") as f:
    passFile = f.read().splitlines()

passSetClean(passFile)

