import socket
import time
import threading, queue
import logging


class Chatserver:

        #static config
        amountConnections = 500
        new_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        new_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_ip = '127.0.0.1'
        port = 8080

        #datastructures
        clientList = []
        accountDict = {}
        messageQueue = queue.Queue(amountConnections)
        writingQueue = queue.Queue(amountConnections)

        def __init__(self):
            #logging init
            logging.basicConfig(filename='app.log', format='%(asctime)s %(process)d-%(levelname)s-%(message)s', filemode='a', level=logging.NOTSET, datefmt='%Y-%m-%d %H:%M:%S')

            #readin useraccounts
            rFile = open("userFile.txt","r")
            for line in rFile:
                user = line.split()
                if user[0] not in self.accountDict.keys():
                    self.accountDict[user[0]] = user[1]
            rFile.close()
            logging.critical('Useraccount data read in complete')

            #starting/binding server
            logging.critical("Server is starting up")
            self.new_socket.bind((self.server_ip,self.port))
            logging.critical('Socket bound successfully')
            logging.critical("Starting server at IP: " + str(self.server_ip))
            self.new_socket.listen(self.amountConnections)
            
            #startingThreads
            beamThread = threading.Thread(None, target=self.beamingThread, name="beamingThread",kwargs=None)
            beamThread.start()
            logging.critical('beaming thread started')
            writer = threading.Thread(None, target=self.writingThread, name="writingThread",kwargs=None)
            writer.start()
            logging.critical('writing thread started')

            while True:
                # take all incomming connections and make a thread out of them
                conn, addr = self.new_socket.accept()
                logging.info('Received connection from: ' + str(addr))
                logging.info('Connection established successful. Connected from:  ' + str(addr))

                self.clientList.append(conn)
                t = threading.Thread(None, target=self.clientThread, name=addr[0], args=((conn, addr)),kwargs=None)
                t.start()
                logging.info("Client thread startet for: " + str(addr))

        def writingThread(self):
            #writes new users into the userfile

            while True:
                    if not self.writingQueue.empty():
                        message = self.writingQueue.get(block=False, timeout=None)
                        wFile = open("userFile.txt","a+")
                        wFile.write(str(message[1])+" "+str(message[2])+"\n")
                        wFile.close()
                        logging.info("Saved new account '" + str(message[1]) + "' to file")
                    else:
                        time.sleep(1)  

        def clientThread(self,conn, addr):
            #each client thread listens to one client

            try:
                message = "Connected to server \r\n"
                conn.send(message.encode('utf8'))
                message = conn.recv(2048).decode('utf8')
                actionList = message.split()
                logging.info(str(addr) + " invoces operation " + str(actionList[0]))
                if actionList[0] == "login":
                    if actionList[1] in self.accountDict.keys():
                        if actionList[2] == self.accountDict[actionList[1]]:
                            logging.info("successfull logged in account '" + str(actionList[1]) + "' with address " + str(addr))
                            pass
                        else:
                            logging.warning("tryed to log into account: '" + str(actionList[1]) + "' with false password")
                            message = "Failed to log into account, please reconnect to try again"
                            conn.send(message.encode('utf8'))
                            message = "incorrect_command"
                            conn.send(message.encode('utf8'))
                            return
                    else:
                        return
                elif actionList[0] == "new" and actionList[1] != "" and actionList[2] != "":
                    if actionList[1] not in self.accountDict:
                        logging.info("created new account '"+ str(actionList[1]) + "'")
                        self.writingQueue.put(actionList)
                        self.accountDict[actionList[1]] = actionList[2]
                else:
                    print(actionList)
                    message = "You tryed to invoce an not supported action, please reconnect to try again"
                    conn.send(message.encode('utf8'))
                    message = "incorrect_command"
                    conn.send(message.encode('utf8'))
                    logging.warning(str(actionList[1]) + "tryed to invoce a false action: " + str(actionList[0]))
                    return
            except:
                logging.critical("User authentification failed")


            while(True):
                message = conn.recv(2048).decode('utf8')
                if len(message) == 0:
                    self.clientList.remove(conn)
                    logging.info("user " + str(addr) + " disconnected")
                    break
                logging.info("user '"+ actionList[1] +"' at "+ str(addr) + " send message: " + str(message))
                self.messageQueue.put((conn,actionList[1], message))


        def beamingThread(self):
            #sends the message to all connected threads

            while True:
                if not self.messageQueue.empty():
                    message = self.messageQueue.get(block=False, timeout=None)
                    conn = message[0]
                    message_to_send = message[1] + " send Message: " + message[2] + "\n"
                    for client in self.clientList:
                        if client != conn:
                            client.send(message_to_send.encode('utf8'))
                else:
                    time.sleep(1)


if __name__ == "__main__":
    server = Chatserver()
