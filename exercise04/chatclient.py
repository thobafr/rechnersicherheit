import select
import socket
import select
import threading, queue
import hashlib

class ChatClient:

	#static config
	clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_ip = '127.0.0.1'
	client_ip = '127.0.0.2'
	sport = 8080
	running = True

	receiveQueue = queue.Queue(50)

	def sendThread(self):
		#thread for sending to server
		print("---> To close the chat use the command ?close? <---")
		while self.running:
			message = input()
			if message == "?close?":
				self.clientSocket.close()
				self.running = False
			else:
				self.clientSocket.send(message.encode('utf8'))

	def receiveThread(self):
		#thread fpr receiving from the server
		while self.running:
			try:
				ready = select.select([self.clientSocket], [], [], 0.1)
				if ready[0]:
					message = self.clientSocket.recv(2048)
					self.receiveQueue.put(message)
			except:
				self.running = False
				break

	def __init__(self):
		
		#connect to server
		self.clientSocket.connect((self.server_ip, self.sport))
		self.clientSocket.setblocking(0)

		#login/register process
		while True:
			print("Please choose 'new' to create a new User or 'login' to login into an existing one.")
			akkuText = input()
			print('', end='')
			if akkuText == "new":
				print("Please input new username:")
				name_Input = input()
				print("Please input new password:")
				password_Input = input()
				password_Input = name_Input + password_Input
				messageHash = hashlib.sha256(password_Input.encode())

				message = (akkuText+" "+name_Input+" "+messageHash.hexdigest())
				
				serverMessage = self.clientSocket.recv(2048)
				self.receiveQueue.put(serverMessage)
				self.clientSocket.send(message.encode('utf8'))
				print()
				break

			elif akkuText == "login":
				print("Please input username:")
				name_Input = input()
				print("Please input password:")
				password_Input = input()
				password_Input = name_Input + password_Input
				messageHash = hashlib.sha256(password_Input.encode())

				message = (akkuText+" "+name_Input+" "+messageHash.hexdigest())
				
				serverMessage = self.clientSocket.recv(2048)
				self.receiveQueue.put(serverMessage)
				self.clientSocket.send(message.encode('utf8'))
				print()
				break
			
		#start handlingThreads
		sendThread = threading.Thread(None, target=self.sendThread, name="sendThread", kwargs=None)
		sendThread.start()
		receiveThread = threading.Thread(None, target=self.receiveThread, name="receiveThread", kwargs=None)
		receiveThread.start()

		#Print and froced quit if command was wrong
		while sendThread.is_alive() and receiveThread.is_alive():
			if not self.receiveQueue.empty():
				message = self.receiveQueue.get(block=False, timeout=None)
				if message.decode('utf8') == "incorrect_command":
					self.clientSocket.close()
					self.running = False
				if message.decode('utf8') == "":
					print("The server disconnected suddenly please try again")
					self.clientSocket.close()
					self.running = False
					break
				print(message.decode('utf8'))


if __name__ == "__main__":
	client = ChatClient()